FROM debian:stable-slim

ENV DEBIAN_FRONTEND noninteractive

RUN date '+%m%d%y' > .docker_build_date

RUN apt update -y && apt install -y eatmydata && eatmydata -- apt dist-upgrade -y

RUN eatmydata -- apt install -y \
        python3 \
        python3-lxml \
        python3-pymysql \
        python3-requests \
        python3-venv \
        python3-wheel \
        python3-yaml \
        netcat-openbsd \
        curl

RUN eatmydata -- apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir -p /srv/pioupiou_to_carbon/config
COPY pioupiou_to_carbon.py /srv/pioupiou_to_carbon
COPY requirements.txt /srv/pioupiou_to_carbon

WORKDIR /srv/pioupiou_to_carbon
RUN eatmydata -- python3 -m venv --system-site-packages .
RUN eatmydata -- /srv/pioupiou_to_carbon/bin/pip3 install -r requirements.txt

CMD /srv/pioupiou_to_carbon/bin/python3 \
    /srv/pioupiou_to_carbon/pioupiou_to_carbon.py \
    /srv/pioupiou_to_carbon/config/config.yaml

