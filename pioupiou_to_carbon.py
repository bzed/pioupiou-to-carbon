#!/usr/bin/env python3

import yaml
import time
import graphyte
import sys
import requests
from datetime import datetime as dt

CONFIG = {}


def update_station(station_id, station_name):
    url = f"http://api.pioupiou.fr/v1/live/{station_id}"
    r = requests.get(url)
    weather_data = r.json()
    weather_data = weather_data["data"]["measurements"]
    measurement_time = dt.fromisoformat(weather_data["date"]).timestamp()

    print("Updating {}/{}: {}".format(station_id, station_name, str(weather_data)))

    measurements = list(weather_data.keys())
    measurements.remove("date")

    for i in measurements:
        if not weather_data[i]:
            continue
        _v = float(weather_data[i])
        metric = f"{station_name}.{i}.value"
        graphyte.send(
            metric,
            _v,
            timestamp=measurement_time,
        )
    return measurement_time


def main():
    keep_running = True
    graphyte.init(**CONFIG["carbon"])

    while keep_running:
        next_update = time.time() + 180

        for _id, _name in CONFIG["stations"].items():
            try:
                next_update = update_station(_id, _name) + 11 * 60
            except KeyboardInterrupt:
                print("Received Keyboard interrupt, exiting")
                keep_running = False
                break
            except Exception as e:
                print("Update of station {}/{} failed: {}".format(_id, _name, e))

        sleep_time = next_update - time.time()
        if sleep_time > 0:
            time.sleep(sleep_time)


if __name__ == "__main__":

    print("Starting pioupiou-to-carbon!")
    if len(sys.argv) < 2:
        print("Usage: {} <configfile>".format(sys.argv[0]))
        sys.exit(1)

    with open(sys.argv[1], "r") as config_file:
        CONFIG = yaml.safe_load(config_file)
    try:
        main()
    except KeyboardInterrupt:
        print("Received Keyboard interrupt, exiting")
